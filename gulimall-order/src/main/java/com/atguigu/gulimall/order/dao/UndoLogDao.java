package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author huchang
 * @email huchang@gmail.com
 * @date 2021-03-28 16:48:15
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
