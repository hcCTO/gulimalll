package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author huchang
 * @email huchang@gmail.com
 * @date 2021-03-28 17:19:39
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
