package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.MemberEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员
 * 
 * @author huchang
 * @email huchang@gmail.com
 * @date 2021-03-28 17:19:39
 */
@Mapper
public interface MemberDao extends BaseMapper<MemberEntity> {
	
}
