package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author huchang
 * @email huchang@gmail.com
 * @date 2021-03-28 14:47:37
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
