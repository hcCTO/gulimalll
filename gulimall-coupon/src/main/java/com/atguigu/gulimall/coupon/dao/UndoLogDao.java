package com.atguigu.gulimall.coupon.dao;

import com.atguigu.gulimall.coupon.entity.UndoLogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author huchang
 * @email huchang@gmail.com
 * @date 2021-03-28 17:09:37
 */
@Mapper
public interface UndoLogDao extends BaseMapper<UndoLogEntity> {
	
}
